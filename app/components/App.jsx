import React from 'react';
import Notes from './Notes.jsx';
import uuid from 'node-uuid';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: [
        {
          id: uuid.v4(),
          task: 'Webpack'
        },
        {
          id: uuid.v4(),
          task: 'React'
        },
        {
          id: uuid.v4(),
          task: 'No Angular'
        }
      ]
    };
    this.findNote = this.findNote.bind(this);
    this.addNote = this.addNote.bind(this);
    this.editNote = this.editNote.bind(this);
    this.deleteNote = this.deleteNote.bind(this);
  }

  render() {
    const notes = this.state.notes;
    return (
      <div>
        <button onClick={this.addNote}>+</button>
        <Notes items={notes} onEdit={this.editNote} onDelete={this.deleteNote}/>
      </div>
    );
  }

  addNote() {
    this.setState({
      notes: this.state.notes.concat([{
        id: uuid.v4(),
        task: 'New task created'
      }])
    });
  }

  editNote(noteId, task) {
    console.log(noteId, task);
    let notes = this.state.notes;
    const noteIndex = this.findNote(noteId);
    if (noteIndex < 0) {
      return;
    }
    notes[noteIndex].task = task;
    this.setState({notes});
    console.log('note edited', noteId, task);
  }

  deleteNote(id) {
    const notes = this.state.notes;
    const noteIndex = this.findNote(id);

    if (noteIndex < 0) {
      return;
    }

    this.setState({
      notes: notes.slice(0, noteIndex).concat(notes.slice(noteIndex + 1))
    });
  }

  findNote(id) {
    let notes = this.state.notes;
    const noteIndex = notes.findIndex((note) => note.id === id);
    if (noteIndex < 0) {
      console.log('Failed to find note', notes, id);
    }
    return noteIndex;
  }
}

export default App;
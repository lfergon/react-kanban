 var path = require('path');
 var HtmlWebpackPlugin = require('html-webpack-plugin');
 var webpack = require('webpack');
 var merge = require('webpack-merge');

 var TARGET = process.env.npm_lifecycle_event;
 var ROOT_PATH = path.resolve(__dirname);

 var common = {
   entry: path.resolve(ROOT_PATH, 'app/main.jsx'),
   output: {
     path: path.resolve(ROOT_PATH, 'build'),
     filename: 'bundle.js'
   }
 };

 if (TARGET === 'start' || !TARGET) {
   module.exports = merge(common, {
     devtool: 'eval',
     devServer: {
       colors: true,
       historyApiFallback: true,
       hot: true,
       inline: true,
       progress: true,
       port: 8000
    },
    module: {
     loaders: [
       {
         test: /\.css$/,
         loaders: ['style', 'css'],
         include: path.resolve(ROOT_PATH, 'app')
       },
       {
         test: /\.jsx?/,
         loader: 'babel',
         include: path.resolve(ROOT_PATH, 'app'),
         exclude: /node_modules/,
         query: {
          presets: ['react']
         }
      }
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        title: 'Kanban app'
      }),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        },
        output: {
          comments: false
        }
      })
    ]
   }); 
 }